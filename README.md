# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://bitbucket.org/svsadmin/stroboskop

```

Naloga 6.2.3:
https://bitbucket.org/svsadmin/stroboskop/commits/741ba135c1d3d32f375355f2a4ee0724cb9eac27

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/svsadmin/stroboskop/commits/f0719471b58c5648a8daf4c99747f7ee4a32cfe5

Naloga 6.3.2:
https://bitbucket.org/svsadmin/stroboskop/commits/268367f3bb1acffac2761aa09630be678e3e3916

Naloga 6.3.3:
https://bitbucket.org/svsadmin/stroboskop/commits/0ad974b179e1114587a25ee22a5c39331f58b49b

Naloga 6.3.4:
https://bitbucket.org/svsadmin/stroboskop/commits/e48e2a2e2430fd90c29fa81ce5a83e7772266fcb

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/svsadmin/stroboskop/commits/0a330455ac49975878b7cd3a36f0c2e459b1c34e

Naloga 6.4.2:
https://bitbucket.org/svsadmin/stroboskop/commits/466ce35359fea91965deacd94ecfd0ae3798380e

Naloga 6.4.3:
https://bitbucket.org/svsadmin/stroboskop/commits/9244aba106fccdd553f3bc99a2c5d3a4129d236f

Naloga 6.4.4:
https://bitbucket.org/svsadmin/stroboskop/commits/46c5c29ca5986e19553caa3a78ec5b5eb526bce5